FROM alpine:3.9
# We'll likely need to add SSL root certificates
RUN apk --no-cache add \
    curl \
    jq
